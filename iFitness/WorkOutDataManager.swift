//
//  WorkOutDataManager.swift
//  iFitness
//
//  Created by mac on 2/20/20.
//  Copyright © 2020 Delta. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


struct coordinate: Codable {
    
    var latitude: Double
    var longtitude: Double
}

struct Workout: Codable {
    
    var endTime: Date
    var duration: TimeInterval
    var locations: [coordinate]
}


typealias Workouts  = [Workout]


class WorkOutDataManager {
    
    static let sharedManager = WorkOutDataManager()
    
    private var workouts: Workouts?
    
    private var activeLocations: [CLLocationCoordinate2D]?
    
    private init() {
        
        print("Singleton initialized.. .")
        loadfromPlist()
    }
    
    private var workoutsFileUrl: URL? {
        guard let documentsURL = documentsDirectoryURL()  else {
            
            return nil
            
        }
        return documentsURL.appendingPathComponent("WorkOuts.plist")
    }
    
    func createNewWorkout() {
        activeLocations = [CLLocationCoordinate2D]()
    }
    
    func addLocation(coordinate:CLLocationCoordinate2D) {
        
        activeLocations?.append(coordinate)
    }
    
    func saveWorkout(duration: TimeInterval) {
        
        guard let activeLocations = activeLocations else {
            
            return
        }
        
        let mappedCoordinates = activeLocations.map{(value: CLLocationCoordinate2D) in
            
            return coordinate(latitude: value.latitude, longtitude: value.longitude)
        }
        
        let currentWorkout = Workout(endTime: Date(), duration: duration, locations: mappedCoordinates)
        
        workouts?.append(currentWorkout)
        saveToPlist()
    }
    
    func getLastWorkout() -> [CLLocationCoordinate2D]? {
        
        guard let workouts = workouts, let lastWorkout = workouts.last  else {
            
            return nil
        }
        
        let locations = lastWorkout.locations.map{(value: coordinate)  in
            
            return CLLocationCoordinate2D(latitude: value.latitude, longitude: value.longtitude)
        }
        
        return locations
    }
    
    func documentsDirectoryURL() -> URL? {
        
        let fileManager = FileManager.default
        
        return fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
    }
    
    func loadfromPlist() {
        
        workouts = [Workout]()
        guard let fileURL = workoutsFileUrl else {
            
            return
        }
        
        do {
            
            let workOutData = try Data(contentsOf: fileURL)
            let decoder = PropertyListDecoder()
            workouts = try decoder.decode(Workouts.self, from: workOutData)
        } catch {
            NSLog("Error reading plist")
        }
    }
    
    func saveToPlist() {
        
        guard let  fileURL = workoutsFileUrl else {
            
            return
        }
        
        
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        
        do{
            let workoutData = try encoder.encode(workouts)
            try workoutData.write(to: fileURL)
        } catch {
            NSLog("Error writing to Plist")
        }
    }
}
