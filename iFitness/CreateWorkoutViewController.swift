//
//  FirstViewController.swift
//  iFitness
//
//  Created by mac on 1/23/20.
//  Copyright © 2020 Delta. All rights reserved.
//

import UIKit
import CoreLocation
import CoreMotion


enum WorkoutState {
    
    case inActice
    case Active
    case Paused
}

let timerInterval: TimeInterval = 1.0

class CreateWorkoutViewController: UIViewController {
    
    // Calculating  WorkOut Timer
    var lastSavedTime: Date?
    var workoutStartTime: Date?
    var pedometer: CMPedometer?
    var workOutDuration: TimeInterval = 0.0
    var workOutTimer: Timer?
    
   
    
    // Calculating WorkOut Distance
    
    var workoutDistance: Double = 0.0
    var lastSavedLocation: CLLocation?
    

    let locationManager = CLLocationManager()
    
    var isMotionAvailable: Bool = false

    
    
    
    
    
    
   
    @IBOutlet weak var WorkOutTimeLabel: UILabel!
    
    
    @IBOutlet weak var WorkoutDistanceLabel: UILabel!
    
    
    @IBOutlet weak var PauseWorkoutButton: UIButton!
    
    @IBOutlet weak var ToggleWorkoutButton: UIButton!
    
    var currentWorkoutState = WorkoutState.inActice
    
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        updateUserInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
   
    ////  Function for " Pause " button
    @IBAction func PauseWorkout(_ sender: UIButton) {
        
       // NSLog("Pause Workout Button Pressed")
        
        switch currentWorkoutState {
            
        case .Paused:
            startWorkout()
            
        case .Active:
            currentWorkoutState = .Paused
            stopWorkOutTimer()
        default:
            NSLog("pauseWorkOut out of Context!!")
        }
        
        updateUserInterface()
    }
    
    
    
    ////  Stop Workout Timer
    
    func stopWorkOutTimer() {
        
        workOutTimer?.invalidate()
        lastSavedTime = nil
        
    }
    
    
    
    //// Toggle -  Stop / Start Button Function
    @IBAction func ToglleWorkout(_ sender: UIButton) {
        
       // NSLog("Toggle Workout Button Pressed")
        
        switch currentWorkoutState {
            
        case .inActice:
           
            requestLocationPermission()
            
        case .Active:
            currentWorkoutState = .inActice
            stopWorkOutTimer()
            
            WorkOutDataManager.sharedManager.saveWorkout(duration: workOutDuration)
        
        default:
            NSLog("ToggleWorkOut() called out of contex")
        }
        //NSLog("Toggle Workout pressed")
        
        updateUserInterface()
    }
    

    /// Function to request Location Permission Before starting the app and the timer Session
    func requestLocationPermission() {
        
        //NSLog("Location Permission Requeseted")
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 10.0 //meters
            
            locationManager.pausesLocationUpdatesAutomatically = true
            locationManager.allowsBackgroundLocationUpdates = true
        
            
            
            locationManager.delegate = self
            
            switch(CLLocationManager.authorizationStatus()) {
                
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .authorizedWhenInUse:
                requestAlwaysPermission()
            case .authorizedAlways:
                startWorkout()
            default:
                presentPermissionAlert()
            }
            
            NSLog("Location Services are available")
            
        }
        else {
            
            presentLocationAlert()
        }
    }
    
    
    func requestAlwaysPermission() {
        
        if let isConfigured = UserDefaults.standard.value(forKey: "isConfigured") as? Bool, isConfigured == true {
            startWorkout()
            updateUserInterface()
        }
        
        else {
            
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    
    func  startWorkout() {
        currentWorkoutState = .Active
        UserDefaults.standard.setValue(true, forKey: "isConfigured")
        UserDefaults.standard.synchronize()
       
       // lastSavedTime = Date()
        let CurrentWorkoutTime  = Date()
       // workoutStartTime = Date()
        if let lastTime = lastSavedTime {
            self.workOutDuration += CurrentWorkoutTime.timeIntervalSince(lastTime)
        }
        
        workoutStartTime = Date()
        WorkOutDataManager.sharedManager.createNewWorkout()
        
       
       // workOutDuration =
        workOutTimer = Timer.scheduledTimer(timeInterval: timerInterval, target: self, selector: #selector(updateWorkoutData), userInfo: nil, repeats: true)
        
        locationManager.startUpdatingLocation()
        
        

        
        //// MARK: - Motion Availability check
        
        if (CMMotionManager().isDeviceMotionAvailable && CMPedometer.isStepCountingAvailable() && CMAltimeter.isRelativeAltitudeAvailable()) {
            
            // .. . Start Motion updates
            
            isMotionAvailable = true
            startPedometerUpdates()
            
        } else {
            
            NSLog("Motion Availability is not available on the device.. .")
            isMotionAvailable = false
        }
        
        
        
    }
    
    func startPedometerUpdates() {

              guard let workoutStartTime = workoutStartTime else {

                  return
              }

              pedometer = CMPedometer()
              pedometer?.startUpdates(from: workoutStartTime, withHandler: {( pedometerData: CMPedometerData?, error: Error?) in
                  NSLog("Received pedometer update.. .")
              })
          }

    
     @objc func updateWorkoutData() {
        
        
    let now  = Date()
        if let lastTime = lastSavedTime {
            self.workOutDuration += now.timeIntervalSince(lastTime)
        }
        
    
        
        
        
        self.workOutDuration += timerInterval
        WorkOutTimeLabel?.text = stringFromTime(timeInterval: self.workOutDuration)
        
       
        WorkoutDistanceLabel.text = String(format: "%.2f meters", arguments: [workoutDistance])
        
    }
    
    
    func stringFromTime(timeInterval: TimeInterval) -> String {
        
        let intDuration = Int(timeInterval)
        let second = intDuration % 60
        let minute = (intDuration / 60) % 60
        let hour = (intDuration / 3600)
        
        if hour > 0 {
            
            return String ("\(hour) hrs \(minute) mins \(second) secs")
        }
        else {
            
            return String ("\(minute) mins \(second) secs")
        }
        
    }
    
    
    
    
    /// Requests Permission to use location services for the app whenever there are some permission error and takes the user to the device's permission settings page
    
    func presentPermissionAlert() {
        
        let Alert = UIAlertController(title: "Permission Error", message: " Allow ifitness to use device's location services", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {
            
            ( action: UIAlertAction) in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(settingsURL) {
                
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            }
        })
        
         let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
    
        
        Alert.addAction(okAction)
        Alert.addAction(cancelAction)
        self.present(Alert,animated: true, completion: nil)
    }
    
    /// Alert appears on the screen to inform the user that the device's location services are disabled and requests the user to activate the service, but this time user has to manually activate the service on device's settings
    
    func presentLocationAlert() {

        let Alert = UIAlertController(title: "Permission Alert", message: "Please Enable Location Services On Your Device", preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)

       // let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)

        Alert.addAction(okAction)
      //  Alert.addAction(cancelAction)
        self.present(Alert,animated: true,completion: nil)

    }
        
    /// Updates user interface and the state of the buttons according to the app's current status and updates the time as well
    
    
    func updateUserInterface() {
        
        switch(currentWorkoutState) {
            
        case .Active:
            ToggleWorkoutButton.setTitle("Stop", for: UIControl.State.normal)
            PauseWorkoutButton.setTitle("Pause", for: UIControl.State.normal)
            PauseWorkoutButton.isHidden = false
            
        case .Paused:
            
            PauseWorkoutButton.setTitle("Resume", for: UIControl.State.normal)
            PauseWorkoutButton.isHidden = false
           
            if currentWorkoutState == .inActice{
                ToggleWorkoutButton.setTitle("Start", for: UIControl.State.normal)
                PauseWorkoutButton.setTitle("Pause", for: UIControl.State.normal)
                workOutDuration = 0.0
                PauseWorkoutButton.isHidden = true
                
            }
            
        default:
            ToggleWorkoutButton.setTitle("Start", for: UIControl.State.normal)
            PauseWorkoutButton.setTitle("Pause", for: UIControl.State.normal)
            workOutDuration = 0.0
            PauseWorkoutButton.isHidden = true
        }
    }
    
    

}

extension CreateWorkoutViewController:
CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager,  didChangeAuthorization status: CLAuthorizationStatus) {
        //NSLog("Received permission update")
        
        switch status {
        case .authorizedWhenInUse:
            requestAlwaysPermission()
        case .authorizedAlways:
            startWorkout()
        case .denied:
            
            presentPermissionAlert()
        default:
        
            NSLog("Unhandled Location Manger Status: \(status)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let mostRecentLocation = locations.last  else {
            
            NSLog("Unable to read the most location")
            
            return
        }
        
        if let savedLocation = lastSavedLocation {
            
            let distanceDelta = savedLocation.distance(from: mostRecentLocation)
            workoutDistance += distanceDelta
        }
        
        lastSavedLocation = mostRecentLocation
        
        NSLog("Most Recent Location: \(String(describing: mostRecentLocation))")
        
        WorkOutDataManager.sharedManager.addLocation(coordinate: mostRecentLocation.coordinate)
    }
}

